    #include <iostream>
    #include <stdio.h>
    #include <stdlib.h>
    #include <time.h>
    #include <cmath>
    #include <vector>
    #include <ilcplex/ilocplex.h>
    #include <chrono>
    #include "pool.h"
    #include "modeling.h"
    #include "writing.h"
    
    ILOSTLBEGIN

    using std::copy;
    using std::cout;
    using std::cerr;
    using std::endl;
    using std::vector;
    using std::min;
    using sec = chrono::seconds;
    using millsec = chrono::milliseconds;
    using get_time = chrono::steady_clock;
    
    vector< vector<int> > pool(int nf);
    void addConstraints(IloEnv env, IloModel model, IloBoolVarArray y, 
                     vector< vector<int> > pool_runs, int nf, int nr, int n0ME, int n0IE);
    void addObjFunct(IloEnv env, IloModel model, IloBoolVarArray y, int sizey, IloObjective obj);
    void writeBasicDesign(vector< vector<int> > sol, int morf[],  int numeraton[]);
    void writeIsomorphisms(vector< vector<int> > pool_runs, vector<int> sol_index);
    void addIsomorphismConstraints(IloEnv env, IloModel model, IloBoolVarArray y);
void setCplexParametersCplex(IloCplex cplex, float time_limit, int symParam, int nodeSel, int heurFreq);
    void writeTimeFile(vector< int> t_sol, vector< int> t_solver, vector< int> t_construction,
                        vector< int> t_iso, long mem_usage, int morf[]);

    #if !defined(ARRAY_SIZE)
        #define ARRAY_SIZE(x) (sizeof((x)) / sizeof((x)[0]))
    #endif



    main(int argc, char* argv[])
    {
        // time limits
        //float time_limit_iter = 3600;
        float time_limit = 3600;
        // vectors to store timings
        // t_solver: time spent by the solver on each iteration
        // t_construction: time spent in problem construction on each iteration
        // t_iso: time spent in finding and writing the isomorphisms to a file
        // t_sol: time until a solution is found, or infeasibility is proven, from the beginning
        vector< int> t_sol, t_solver, t_construction, t_iso;
        // start clock
        auto t_start = get_time::now();
        // long to store memory usage
        long mem_usage;
        // numeration for the basic designs
        // numeration[0] : for a given nf (general index)
        // numeration[1] : for a given nf, nr
        // numeration[2] : for a given nf, nr, n0ME
        // numeration[3] : for a given nf, nr, n0ME, n0Ie
        int numeration[] = {0, 0, 0, 0};
	bool optimize = false;
	
        // just for debugging
        int deb_nf = 5;
        int deb_nr = 24;
        int deb_n0ME = 10;
        int deb_n0IE = 16;
        int morf_deb[] = {deb_nf, deb_nr, deb_n0ME, deb_n0IE};
        
        // check number of parameters
        if (argc < 5)
        {
            cout << "Invalid number of parameters: " << argc << '\n';
            return 1;
        }
	deb_nf = strtol(argv[1], NULL, 0);
	deb_nr = strtol(argv[2], NULL, 0);
	deb_n0ME = strtol(argv[3], NULL, 0);
	deb_n0IE = strtol(argv[4], NULL, 0);
	optimize = strtol(argv[5], NULL, 0);
	// int symParam = strtol(argv[6], NULL, 0);
	// int nodeSel = strtol(argv[7], NULL, 0);
	// int heurFreq = strtol(argv[8], NULL, 0);
	
        // parse the number of factors, number of runs
        int nf = strtol(argv[1], NULL, 0);
        ////////////////
        ///   DEBUG  ///
        ////////////////
        nf = deb_nf;
        int nruns_array [2] = {strtol(argv[2], NULL, 0), strtol(argv[3], NULL, 0)};
        int nrows = std::pow(3,nf)-1;
        std::vector <int> nruns(nruns_array, nruns_array + ARRAY_SIZE(nruns_array));
        // construct pool_runs (row major order)
        std::vector< std::vector<int> > pool_runs;
        pool_runs = pool(nf);       
        // loop for number of runs
       for(int nr = deb_nr ; nr < deb_nr+1 ; nr++)
//         for(int nr = nruns[0] ; nr < nruns[1]+1 ; nr = nr + 2)
        {
            // initialize numeration
            numeration[1] = 0;
            // generate n0Me, n0Ie pairs
            int max_n0ME = nr - 4;
            int initial_n0ME = nr%2 + 2;
//             for(int n0ME = initial_n0ME ; n0ME < max_n0ME ; n0ME = n0ME + 2)
            for(int n0ME = deb_n0ME ; n0ME < deb_n0ME+1 ; n0ME++)
            {
                // initialize numeration
                numeration[2] = 0;
                int max_n0IE = min(2*n0ME-1, nr-2);
              for(int n0IE = deb_n0IE ; n0IE < deb_n0IE+1 ; n0IE++)
//                 for(int n0IE = n0ME+2 ; n0IE < max_n0IE + 2 ; n0IE = n0IE + 2)
                {
                    // int[] morf 
                    int morf[] = {nf, nr, n0ME, n0IE};
                    // initialize numeration
                    numeration[3] = 0;
                    // start clock
                    auto t_start = get_time::now();
                    
                    bool goOn = true;
                    bool solution_attained = false;
                    
                    // create environment and model
                    IloEnv env;
                    try
                    {
                        // initialize t_construction start time
                        auto t_start_construction = get_time::now();
                        // INITIALIZATION -- solve a first model without isomorphism constraints
                        IloModel model(env);
                        IloBoolVarArray y(env, nrows);
                        // add constraints
                        addConstraints(env, model, y, pool_runs, nf, nr, n0ME, n0IE);
                        // objective
                        IloObjective obj = IloMinimize(env);
			addObjFunct(env, model, y, nrows, obj);                     
                        // Optimize
                        IloCplex cplex(model);
                        //cplex.setOut(env.getNullStream());
                        //cplex.setWarning(env.getNullStream());
                        // stopping parameter stop
                        // stop = 0 : beginning or right after adding a set of isomorphism constraints
                        // stop = 1 : integral solution found
                        // stop = 2 : time limit exceeded -- end of algorithm
                        int stop = 0;
                        bool round = true;
                        while(stop < 2 && optimize)
                        {
                            //////////////////////////////////
                            // add isomorphism constraints ///
                            //////////////////////////////////
                            if(stop == 1)
                            {
                                std::cout << "adding isomorphism constraints\n";
                                addIsomorphismConstraints(env, model, y);
                            }

			    auto t_spent = get_time::now() - t_start;
			    float time_limit_iter = time_limit - chrono::duration_cast<sec>(t_spent).count();
			    
                            //setCplexParametersCplex(cplex, time_limit_iter, symParam, nodeSel, heurFreq);
                            // retrieve construction time
                            auto t_construction_iter = get_time::now() - t_start_construction;
                            t_construction.push_back(chrono::duration_cast<millsec>(t_construction_iter).count());
                            //////////////////////////////
                            // initialization -- solve ///
                            //////////////////////////////
                            // initialize t_solver
                            auto t_start_solver = get_time::now();
                            if( cplex.solve() == IloTrue )
                            {
                                // retrieve solver time, sol time
                                auto t_solver_iter = get_time::now() - t_start_solver;
                                auto t_sol_iter = get_time::now() - t_start;
                                t_solver.push_back(chrono::duration_cast<millsec>(t_solver_iter).count());
                                t_sol.push_back(chrono::duration_cast<millsec>(t_sol_iter).count());
                                // retrieve solution, in initialization we use two solution arrays: sol & sol0, to later check cycling
                                IloNumArray sol(env);
                                cplex.getValues(sol, y);     
                                stop = 1;  
                                // update numeration
                                for(int i = 0 ; i < 4 ; i++)
                                    numeration[i]++;
                                // retrieve design rows
                                vector<int> sol_index;
                                vector< vector<int> > sol_design;
                                for(int t = 0 ; t < nrows ; t++)
                                {
                                    if(sol[t] > 0.001){
                                        sol_index.push_back(t);
                                        sol_design.push_back(pool_runs[t]);
                                    }
                                }
                                // print solution to file
                                writeBasicDesign(sol_design, morf, numeration);
                                // retrieve t_iso
                                auto t_start_iso = get_time::now();
                                // print isomorphisms
                                writeIsomorphisms(pool_runs, sol_index);
                                auto t_iso_iter = get_time::now() - t_start_iso;
                                t_iso.push_back(chrono::duration_cast<millsec>(t_iso_iter).count());
                                // check time, if > time_limit then stop algorithm
                                auto time_check = get_time::now() - t_start;
                                if(chrono::duration_cast<sec>(time_check).count() > time_limit)
                                    stop = 2;

				//////////////////////
				////// DEBUG ////////
				// just one time
				//stop = 2;
                            } else{
                                // retrieve solver time, sol time
                                auto t_solver_iter = get_time::now() - t_start_solver;
                                auto t_sol_iter = get_time::now() - t_start;
                                t_solver.push_back(chrono::duration_cast<millsec>(t_solver_iter).count());
                                t_sol.push_back(chrono::duration_cast<millsec>(t_sol_iter).count());
                                // stop the while loop
                                stop = 2;
                            }
                        }
			// write mps file
			std::ostringstream oss;
			oss << "model_" << deb_nf << "_" << deb_nr << "_" << deb_n0ME << "_" << deb_n0IE << ".mps";
			cplex.exportModel(oss.str().c_str());

                    }
                    catch (IloException& ex) {
                        cerr << "Error: " << ex << endl;
                    }
                    catch (...) {
                        cerr << "Error" << endl;
                    }
                    env.end();
                }             
            }   
        }
        
//         // CHECK 
//         cout << '\n';
//         for(int s = 0 ; s < pool_runs.size() ; s++)
//         {
//             cout << '\n';
//             for (int i = 0 ; i < nf ; i++)
//                 cout << pool_runs[s][i] << ' ';
//         }
        // display total time on screen
        auto t_end = get_time::now();
        auto diff = t_end - t_start;
        auto diff_millsec = chrono::duration_cast<millsec>(diff).count();
        std::cout << "\nthis is the time (in milliseconds): " << diff_millsec << "\n";
        // write time vectors to file
        writeTimeFile(t_sol, t_solver, t_construction, t_iso, mem_usage, morf_deb);        
        return 0;
    }


