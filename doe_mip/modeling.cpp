    #include <iostream>
    #include <fstream>
    #include <string>
    #include <stdio.h>
    #include <stdlib.h>
    #include <cmath>
    #include <vector>
    #include <algorithm>
    #include <random>
    #include <ilcplex/ilocplex.h>
    #include "modeling.h"
    
    using std::vector;
    
    void addConstraints(IloEnv env, IloModel model, IloBoolVarArray y, 
                     vector< vector<int> > pool_runs, int nf, int nr, int n0ME, int n0IE)
    {
        int nrows = std::pow(3,nf)-1;
        // constraints 1: number of runs = nr
        model.add(IloSum(y) == nr);
        // constraints 2: sparsity n0ME
        // constraints 4: first order moments
        for(int i = 0 ; i < nf ; i++)
        {
            IloNumExpr c2(env);
            IloNumExpr c4(env);
            for(int t = 0 ; t < nrows ; t++)
            {
                if(pool_runs[t][i] == 0)
                    c2 += y[t];
                c4 += pool_runs[t][i] * y[t];
            }
            model.add( c2 == n0ME);
            model.add( c4 == 0);
        }
        // constraints 3: sparsity n0IE
        // constraints 5: second order moments
        for(int i = 0 ; i < nf-1 ; i++)
        {
            for(int j = i+1 ; j < nf ; j++)
            {
                IloNumExpr c3(env);
                IloNumExpr c5(env);
                for(int t = 0 ; t < nrows ; t++)
                {
                    if(pool_runs[t][i] == 0 || (pool_runs[t][j] == 0))
                        c3 += y[t];
                    int coeff = pool_runs[t][i] * pool_runs[t][j];
                    c5 += coeff * y[t];
                }
                model.add( c3 == n0IE);
                model.add( c5 == 0);
            }
        }
        // constraints 6: third order moments
        for(int i = 0 ; i < nf-1 ; i++)
        {
            for(int j = i ; j < nf ; j++)
            {
                for(int k = j ; k < nf ; k++)
                {
                    if(i < k)
                    {
                        IloNumExpr c6(env);
                        for(int t = 0 ; t < nrows ; t++)
                        {
                            int coeff = pool_runs[t][i] * pool_runs[t][j] * pool_runs[t][k];
                            c6 += coeff * y[t];
                        }
                        model.add(c6 == 0);
                    }
                }
            }
        }
        
    }
    void addObjFunct(IloEnv env, IloModel model, IloBoolVarArray y, int sizey, IloObjective obj)
    {
        
        for(int i = 0 ; i < sizey ; i++)
            obj.setLinearCoef(y[i], 1.);
        model.add(obj);
    }
    
    void addIsomorphismConstraints(IloEnv env, IloModel model, IloBoolVarArray y)
    {
        // read isomorphism file
        std::ifstream infile("isomorphisms");
        std::string line;
        while(std::getline(infile, line))
        {
            std::vector<int> vect;
            std::istringstream iss(line);
            while( iss.good() )
            {
                std::string substr;
                std:getline( iss, substr, ',');
                int index;
                std::stringstream(substr) >> index;
                vect.push_back( index );
            }
            // add constraint
            IloNumExpr c_iso(env);
            for(int i = 0 ; i < vect.size() ; i++)
                c_iso += y[vect[i]];
            IloInt rhs = vect.size() - 1;
            model.add(c_iso <= rhs);
        }
        std::cout << '\n';
        
    }

    std::vector<double> perturbationStep(IloNumArray sol, std::vector<double> sol_rounded, int nrows)
    {
        // generate random object, apply the perturbation move
        std::random_device rd;
        std::mt19937 rng(rd());
//         // check
//         std::cout << "\nbefore\n";
//         for(int i = 0 ; i < nrows ; i++)
//         {
//             std::cout << sol_rounded[i] << " ";
//         }
//         std::cout << "\n";
        
        for(int i = 0 ; i < nrows ; i++)
        {
            std::uniform_real_distribution<double> uni(-0.3, 0.7);
            double random_pert = uni(rng);
            double coeff = std::abs(sol[i] - sol_rounded[i]) + std::max(random_pert, 0.);
            if(coeff > 0.5)
                sol_rounded[i] = 1 - sol_rounded[i];       
        }        
//         // check
//         std::cout << "\nafter\n";
//         for(int i = 0 ; i < nrows ; i++)
//         {
//             std::cout << sol_rounded[i] << " ";
//         }
//         std::cout << "\n";
        
        return sol_rounded;
    }    
    
    
void setCplexParametersCplex(IloCplex cplex, float time_limit, int symParam, int nodeSel, int heurFreq)
    {
        // verbosity
        cplex.setParam(IloCplex::Param::Tune::Display, 3);
        // time limit
        cplex.setParam(IloCplex::Param::TimeLimit, time_limit);
        cplex.setParam(IloCplex::Param::MIP::Limits::Solutions, 1);
        cplex.setParam(IloCplex::Param::Emphasis::MIP, 1);
        // primal simplex method
        //cplex.setParam(IloCplex::Param::RootAlgorithm, 1);
        // symmetry breaking
        cplex.setParam(IloCplex::Param::Preprocessing::Symmetry, symParam);
	cplex.setParam(IloCplex::Param::MIP::Strategy::NodeSelect, nodeSel);
	cplex.setParam(IloCplex::Param::MIP::Strategy::HeuristicFreq, heurFreq);
        // limit the number of iterations
        //cplex.setParam(IloCplex::Param::Simplex::Limits::Iterations, 20000);
        // set the optimality tolerance (default is 1e-6)
        //cplex.setParam(IloCplex::Param::Simplex::Tolerances::Optimality, 0.0001);
        // set the feasibility tolerance (default is 1e-6)
        //cplex.setParam(IloCplex::Param::Simplex::Tolerances::Feasibility, 0.0001);
    }
    
    
    std::vector<double> roundingUpOnlynr(IloNumArray sol, std::vector<double> sol_rounded, int nr, int nrows)
    {
        // set all elements of sol_rounded to zero
        for(int i = 0 ; i < sol_rounded.size() ; i++)
            sol_rounded[i] = 0;
        // sort entries |sol - round(sol)|
//         std::cout << "\nsort entries |sol - round(sol)|" << "\n";
        std::vector<double> abs_diff;
        std::vector<int> indexes;
        //std::cout << sol_rounded.size();
//         std::cout << "nrows: " << nrows << "\n";
        for(int i = 0 ; i < nrows ; i++)
        {
            double coeff = std::abs (sol[i] - sol_rounded[i]);
            abs_diff.push_back(coeff);
            indexes.push_back(i);
        }
//         // check sort
// //         std::cout << "\nbefore sorting\n";
//         for(int i = 0 ; i < nrows ; i++)
//             std::cout << indexes[i] << "  " << sol[indexes[i]] << "  " << sol_rounded[indexes[i]] << "  " << abs_diff[indexes[i]] << "\n";
//         std::cout << "\nhere the sorting is actually taking place" << "\n";
        std::sort( std::begin(indexes), std::end(indexes), [&](int x, int y){return abs_diff[x] > abs_diff[y];} );
//         // check sort
//         std::cout << "\ncheck sort\n";
//         for(int i = 0 ; i < nrows ; i++)
//             std::cout << indexes[i] << "  " << sol[indexes[i]] << "  " << sol_rounded[indexes[i]] << "  " << abs_diff[indexes[i]] << "\n";
        // flip the entries
        std::cout << "\nset to one the nr largest entries" << "\n";
//         // check
//         std::cout << "\nbefore\n";
//         for(int i = 0 ; i < nrows ; i++)
//         {
//             int ind = indexes[i];
//             std::cout << sol_rounded[ind] << " ";
//         }
//         std::cout << "\n";
        for(int i = 0 ; i < nr ; i++)
            sol_rounded[indexes[i]] = 1;
//         // check
//         std::cout << "\nafter\n";
//         for(int i = 0 ; i < nrows ; i++)
//         {
//             int ind = indexes[i];
//             std::cout << sol_rounded[ind] << " ";
//         }
//         std::cout << "\n";
        return sol_rounded;
    }
    
    void antiStallingStep(IloNumArray sol, std::vector<double> sol_rounded, int flipped_terms, int nrows)
    {
        // generate random number of entries to be flipped_terms
//         std::cout << "\ngenerate random number of entries to be flipped_terms" << "\n";
        int range_flip[] = {flipped_terms, 3*flipped_terms/2};
        std::random_device rd;
        std::mt19937 rng(rd());
        std::uniform_int_distribution<int> uni(flipped_terms/2, 3*flipped_terms/2);
        int random_flip = uni(rng);
//         std::cout << "\nrandom number of flipped entries: " << random_flip << "\n";
        // sort entries |sol - round(sol)|
//         std::cout << "\nsort entries |sol - round(sol)|" << "\n";
        std::vector<double> abs_diff;
        std::vector<int> indexes;
        //std::cout << sol_rounded.size();
//         std::cout << "nrows: " << nrows << "\n";
        for(int i = 0 ; i < nrows ; i++)
        {
            double coeff = std::abs (sol[i] - sol_rounded[i]);
            abs_diff.push_back(coeff);
            indexes.push_back(i);
        }
//         // check sort
// //         std::cout << "\nbefore sorting\n";
//         for(int i = 0 ; i < nrows ; i++)
//             std::cout << indexes[i] << "  " << sol[indexes[i]] << "  " << sol_rounded[indexes[i]] << "  " << abs_diff[indexes[i]] << "\n";
//         std::cout << "\nhere the sorting is actually taking place" << "\n";
        std::sort( std::begin(indexes), std::end(indexes), [&](int x, int y){return abs_diff[x] > abs_diff[y];} );
//         // check sort
//         std::cout << "\ncheck sort\n";
//         for(int i = 0 ; i < nrows ; i++)
//             std::cout << indexes[i] << "  " << sol[indexes[i]] << "  " << sol_rounded[indexes[i]] << "  " << abs_diff[indexes[i]] << "\n";
        // flip the entries
        std::cout << "\nflip the entries" << "\n";
        // check
        std::cout << "\nbefore\n";
        for(int i = 0 ; i < nrows ; i++)
        {
            int ind = indexes[i];
            std::cout << sol_rounded[ind] << " ";
        }
        std::cout << "\n";
        for(int i = 0 ; i < random_flip ; i++)
            sol_rounded[indexes[i]] = 1 - sol_rounded[indexes[i]];
        // check
        std::cout << "\nafter\n";
        for(int i = 0 ; i < nrows ; i++)
        {
            int ind = indexes[i];
            std::cout << sol_rounded[ind] << " ";
        }
        std::cout << "\n";
        
    }
        
    
