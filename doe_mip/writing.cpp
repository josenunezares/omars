    #include <iostream>
    #include <fstream>
    #include <sstream>
    #include <vector>
    #include <iterator>
    #include <algorithm>
    #include <unordered_map>
    #include "writing.h"
    #include "pool.h"
    
    using std::vector;
    using std::string;
    using std::unordered_map;
    
    int variations_w_rep_binary(vector<int>* vecPtr, int nf);
    std::unordered_map<string, int> includeIsomorphism(std::unordered_map<string,int> pool_hash, 
                                                        vector< vector<int> > sol_rows, vector<int> inv, 
                                                        int perm[], std::unordered_map<string,int> isomorphisms);
    
    void writeVectorEntries(vector< int> vect, std::string description, std::ofstream& myfile)
    {
        myfile << description;
        for(int i = 0 ; i < vect.size() ; i++)
        {
            myfile << "," << vect[i];
        }
        myfile << '\n';
    }
    
    void writeBasicDesign(vector< vector<int> > sol, int morf[], int numeration[])
    {
        std::ofstream myfile;
        std::stringstream filename;
        std::string rel_path = "./basicDesigns/";
        filename << "basicDesign" << morf[0] << "_" << morf[1] << "_" << morf[2] << "_" << morf[3] << "_"
                        << numeration[0] << "_" << numeration[1] << "_" << numeration[2] << "_" << numeration[3];
        std::string path_file = rel_path + filename.str();
        myfile.open(path_file.c_str());
        for(int s = 0 ; s < sol.size() ; s++)
        {
            for (int i = 0 ; i < sol[0].size() ; i++)
                myfile << sol[s][i] << ' ';
            myfile << '\n';
        }
        myfile.close();
    }
    
    void writeIsomorphisms(vector< vector<int> > pool_runs, vector<int> sol_index)
    {
        /////////////////////////////////////////////////////////////////
        // pool_hash: hashmap with pool_runs
        std::unordered_map<string, int> pool_hash;
        for(int i = 0 ; i < pool_runs.size() ; i++)
        {
            std::stringstream row;
            std::copy(pool_runs[i].begin(), pool_runs[i].end(), std::ostream_iterator<int>(row, ""));
//             std::cout << row.str().c_str() << "\n";
            pool_hash.insert(std::make_pair(row.str().c_str(), i));
        }
        /////////////////////////////////////////////////////////////////
        // order sol_index
        std::sort(sol_index.begin(), sol_index.end());
//         for(vector<int>::iterator it = sol_index.begin() ; it != sol_index.end() ; it++)
//             std::cout << ' ' << *it;
//         std::cout << '\n';
        /////////////////////////////////////////////////////////////////
        // iso_hash: hashtable with sol_indexes (empty)
        std::unordered_map<string, int> isomorphisms;
//         // check
//         if(isomorphisms.find("olalala") == isomorphisms.end())
//             std::cout << "not existing key";
        /////////////////////////////////////////////////////////////////
        // find isomorphisms and check for each if it is in iso_hash
        // col_inv : different ways of multiplying a column (or not) by -1
        int nf = pool_runs[0].size();
        vector< vector<int> > col_inv;
        vector<int> *varPtr = new vector<int>(nf, -1);
        vector<int> *var0 = new vector<int>(nf, -1);
        col_inv.push_back((*var0));
        while(variations_w_rep_binary(varPtr, nf) < 1)
        {
            // copy the vector
            vector<int> var;
            for(int i = 0 ; i < nf ; i++)
                var.push_back((*varPtr)[i]);

            col_inv.push_back(var);
        }

	//vector<int> *debug = new vector<int>(nf, 1);
	//col_inv.push_back(*debug);

	
        // for each subset in col_inv, we try all permutations and add the isomorphisms
        vector< vector<int> > sol_rows;
        for(int i = 0 ; i < sol_index.size() ; i++)
            sol_rows.push_back(pool_runs[sol_index[i]]);
        for(int i = 0 ; i < col_inv.size() ; i++)
        {
            // first the non permutation
            int perm[nf];
            for(int i = 0 ; i < nf ; i++)
                perm[i] = i;
            do {
                isomorphisms = includeIsomorphism(pool_hash, sol_rows, col_inv[i], perm, isomorphisms);
            } while(std::next_permutation(perm, perm+nf) );
        }
        /////////////////////////////////////////////////////////////////
        // print all isomorphisms to file (only one file)
        std::ofstream myfile;
        myfile.open("isomorphisms");
        for(auto kv : isomorphisms){
            std::string aux = kv.first;
            aux.pop_back();
            myfile << aux << "\n";
        }
        myfile.close();        
        // check
       // std::cout << '\n' << "isomorphism unordered_map" << "\n";
        for(auto kv : isomorphisms)
        {
            std::string aux = kv.first;
            aux.pop_back();
            //std::cout << aux << '\n';
        }
        
    }
    
    void writeTimeFile(vector< int> t_sol, vector< int> t_solver, vector< int> t_construction,
                       vector< int> t_iso, long mem_usage, int morf[])
    {
        std::ofstream myfile;
        std::stringstream filename;
        std::string rel_path = "./performance/";
        filename << "timing" << morf[0] << "_" << morf[1] << "_" << morf[2] << "_" << morf[3]; 
        std::string path_file = rel_path + filename.str();
        myfile.open(path_file.c_str());
        writeVectorEntries(t_sol, "t_sol", myfile);
        writeVectorEntries(t_solver, "t_solver", myfile);
        writeVectorEntries(t_construction, "t_construction", myfile);
        writeVectorEntries(t_iso, "t_iso", myfile);
//         myfile << "memory usage: " << mem_usage << '\n';
        myfile.close();
        
        
    }
    
