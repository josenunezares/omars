    #include <iostream>
    #include <stdio.h>
    #include <stdlib.h>
    #include <cmath>
    #include <vector>
    #include <iterator>
    #include <algorithm>
    #include <unordered_map>
    #include <sstream>
    #include "pool.h"

    using std::copy;
    using std::cout;
    using std::endl;
    using std::vector;
    using std::string;
    using std::unordered_map;
    using std::min;
    
    
    
    
    int variations_w_rep(vector<int>* vecPtr, int nf)
    {
    //     // CHECK 
    //     cout << '\n';
    //     for (int i = 0 ; i < nf ; i++)
    //         cout << (*vecPtr)[i] << ' ';
        // easy case, increase rithtmost element
        if((*vecPtr)[nf-1] < 1)
        {
            (*vecPtr)[nf-1]++;
            return 0;
        }
        // find the rightmost element to increase and reset right-hand elements
        int j;
        for(j = nf-2 ; j >-1; j--)
        {
            (*vecPtr)[j+1] = -1;
            if((*vecPtr)[j] < 1)
                break;
        }
        // terminate if all elements are 2
        if(j < 0) 
            return 1;
        // increase
        (*vecPtr)[j]++;
        return 0;
    }
    
    vector< vector<int> > pool(int nf)
    {
        vector< vector<int> > pool_runs;
        int nrows = std::pow(3,nf)-1;
        // initialize one vector of size nf
        vector<int> *varPtr = new vector<int>(nf, -1);
        vector<int> *var0 = new vector<int>(nf, -1);
        pool_runs.push_back((*var0));
        int counter = 0;
        while(variations_w_rep(varPtr, nf) < 1)
        {
            if(counter != nrows/2 - 1)
            {
                // copy the vector (except the center run)
                vector<int> var;
                for(int i = 0 ; i < nf ; i++)
                    var.push_back((*varPtr)[i]);
                pool_runs.push_back(var);
            }
            counter++;
        }
        return pool_runs;
    }
    
    int variations_w_rep_binary(vector<int>* vecPtr, int nf)
    {
    //     // CHECK 
    //     cout << '\n';
    //     for (int i = 0 ; i < nf ; i++)
    //         cout << (*vecPtr)[i] << ' ';
        // easy case, increase rithtmost element
        if((*vecPtr)[nf-1] < 1)
        {
            (*vecPtr)[nf-1] = 1;
            return 0;
        }
        // find the rightmost element to increase and reset right-hand elements
        int j;
        for(j = nf-2 ; j >-1; j--)
        {
            (*vecPtr)[j+1] = -1;
            if((*vecPtr)[j] < 1)
                break;
        }
        // terminate if all elements are 2
        if(j < 0) 
            return 1;
        // increase
        (*vecPtr)[j] = 1;
        return 0;
    }
    
    std::unordered_map<string, int> includeIsomorphism(std::unordered_map<string,int> pool_hash, 
                                                    vector< vector<int> > sol_rows, vector<int> inv, 
                                                    int perm[], std::unordered_map<string, int> isomorphisms)
    {
        int nf = inv.size();
        vector<int> iso;
        for(int i = 0 ; i < sol_rows.size() ; i++)
        {
            vector<int> row(nf);
            for(int j = 0 ; j < nf ; j++)
                row[perm[j]] = inv[j] * sol_rows[i][j];
            // retrieve the index of the sol_rowsstd::stringstream row_str;
            std::stringstream row_str;
            std::copy(row.begin(), row.end(), std::ostream_iterator<int>(row_str, ""));
            std::unordered_map<string,int>::const_iterator got = pool_hash.find(row_str.str().c_str());
            iso.push_back(got->second);
        }
        // order iso runs 
        std::sort(iso.begin(), iso.end());
        // check if it is in isomorphisms
        std::stringstream iso_str;
        std::copy(iso.begin(), iso.end(), std::ostream_iterator<int>(iso_str,","));
        std::unordered_map<string,int>::const_iterator got = isomorphisms.find(iso_str.str().c_str());
        if( got == isomorphisms.end() )
        {
            isomorphisms.insert(std::make_pair(iso_str.str().c_str(), 0));
        }
        return isomorphisms;
        
    }
                